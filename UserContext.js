import React from 'react';

// create a Context object for state management
const UserContext = React.createContext()

// create a context provider that allows for consuming components to subscribe to
// context changes
export const UserProvider = UserContext.Provider

export default UserContext;