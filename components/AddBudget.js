import { useState, useEffect, useContext } from 'react'
import { Form, Button, Container, Row, Col, Card} from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../UserContext'
import AddCategory from './AddCategory'
import Swal from 'sweetalert2'

export default function AddBudget() {
    
    const {user, balance, setBalance } = useContext(UserContext)
    
    const [categories, setCategories] = useState([])
    const [name, setName] = useState('')
    const [categoryId, setCategoryId] = useState('')
    const [type, setType] = useState('')
	const [amount, setAmount] = useState(0)
    const [description, setDescription] = useState('')
    
    function createBudget(e){
        if(name !== "" && categoryId !== "" && type !== "" && amount !== ""){
            e.preventDefault()
            const newBalance = () => {
                if(type === "Expense"){
                    return +balance - +amount
                } else {
                    return +balance + +amount
                }
            }
            
            const balanceToServer = newBalance()
    
            fetch(`${process.env.NEXT_PUBLIC_API_URL}/budgets`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    name: name,
                    categoryId: categoryId,
                    type: type,
                    amount: amount,
                    description: description
                    
                })
            })
            .then(res => res.json())
            .then(data => {
                
                if(data===false){
                    Swal.fire('Data Error', 'Something went wrong when fetching the data', 'error')
                }else{
                    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${data}`, {
                        method: "PATCH",
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        },
                        body: JSON.stringify({
                            balance: balanceToServer
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        if(data===false){
                            Swal.fire('Data Error','Something went wrong when fetching the data','error')
                        }else{
                            setBalance(data.balance)
                            Swal.fire('New Budget Added', 'success')
                            Router.push('/budgets')
                        }
                    })
                }
            })
        }
    }

    const handleOnChange = (e) => {
        if(e.target.name === "budgetName"){
            setName(e.target.value)
        } else if (e.target.name === "categoryId") {
            setCategoryId(e.target.value)
        } else if (e.target.name === "categoryType"){
            setType(e.target.value)
        } else if (e.target.name === "amount") {
            setAmount(e.target.value)
        } else if (e.target.name === "description") {
            setDescription(e.target.value)
        }
    }

    useEffect(()=> {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data)
            setCategories(data)
            
        })


    }, [])

    return(
        <Card className="my-3 shadow">
            <Card.Body>
            <Card.Title className="font-weight-bold">Create Budget</Card.Title>
            <Form onSubmit={createBudget}>
                <Form.Group>
                    <Form.Label>Budget Name</Form.Label>
                    <Form.Control type="text" value={name} name="budgetName" onChange={handleOnChange} required/>
                </Form.Group>
                <Form.Group>
                    <Row>
                        <Col xs={7}>
                            <Form.Label>Category</Form.Label>
                            <Form.Control as="select" value={categoryId} name="categoryId" onChange={handleOnChange} required>
                                <option value="" disabled></option>
                            {	categories.length > 0 
                                ? categories.map(category => {
                                return (
                                        <option key = {category._id} value={category._id}>{category.name}</option>
                                    )
                            }) : 
                                <option>No Categories Listed Yet</option>
                            }
                            </Form.Control>
                        </Col>
                        <Col xs={5}>
                            <AddCategory setCategories={setCategories}/>
                            
                        </Col>
                    </Row>
                </Form.Group>
                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Category Type</Form.Label>
                            <Form.Control as="select" value={type} name="categoryType" onChange={handleOnChange} required>
                                <option value="" name="" disabled></option>
                                <option value="Income" name="Income">Income</option>
                                <option value="Expense" name="Expense">Expense</option>
                            </Form.Control>
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group>
                            <Form.Label>Amount</Form.Label>
                            <Form.Control type="number" value={amount} name="amount" onChange={handleOnChange} required/>
                        </Form.Group>
                    </Col>
                </Row>
                
                
                <Form.Group>
                    <Form.Label>Description</Form.Label>
                    <Form.Control as="textarea" rows="3" value={description} name="description" onChange={handleOnChange}/>
                </Form.Group>
                <Button variant="primary" type="submit" className="button2 my-3 font-weight-bold">Submit</Button>
            </Form>
                
            </Card.Body>
        </Card>
    )
}
