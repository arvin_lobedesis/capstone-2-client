import {Modal, Button, Form} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Router from 'next/router'
import Swal from 'sweetalert2'

export default function AddCategory({setCategories}) {

    const [show, setShow] = useState(false);
    const {user} = useContext(UserContext)
    
    
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true);

    const [name, setName] = useState('')
    
    function createCategory(e){
        e.preventDefault()

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name
            })
        }

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories`, payload)
        .then(res => res.json())
        .then(data => {
            if(data.length > 0){
                setCategories(data)
                Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Category Added',
                    showConfirmButton: false,
                    timer: 1000
                  })
                
            }
        })
    }

    return(
        <>
            <a className="font-weight-bold add-icon" onClick={handleShow}><AddCircleOutlineIcon style={{fontSize: 40}}/>Add Category</a>
                

            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                size="sm"
                centered
            >
                <Modal.Header closeButton>
                <Modal.Title>Add Category</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={(e) => createCategory(e)}>
                        <Form.Group>
                            <Form.Label>Category Name</Form.Label>
                            <Form.Control type="text" value={name} placeholder="Ex: Salary, Savings, Bills" onChange={e => setName(e.target.value)} required/>
                        </Form.Group>
                        
                        <Button variant="primary" type="submit" className="button2 font-weight-bold" block>Submit</Button>
                    </Form>
                </Modal.Body>
                
            </Modal>
        </>

    )
    
};
