import {Modal, Button, Form} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Router from 'next/router'
import Swal from 'sweetalert2'

export default function AddIncome() {
    
    const [show, setShow] = useState(false);
    const {user} = useContext(UserContext)
    const budget = user.budgets
    
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true);

	const [description, setDescription] = useState('')
    const [amount, setAmount] = useState(0)

    function addIncome(e){
        e.preventDefault()

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/budgets/incomes`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                description: description,
                amount: amount
                
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data===true){
                Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Income Added',
                    showConfirmButton: false,
                    timer: 1000
                  })
                Router.reload()
            }
        })
    }

    return(
        <>
            <AddCircleOutlineIcon className="add-icon" onClick={handleShow}/>
                

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                size="sm"
                centered
            >
                <Modal.Header closeButton>
                <Modal.Title>Add Income</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={(e) => addIncome(e)}>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" value={description} placeholder="Ex: Salary, Savings," onChange={e => setDescription(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Amount</Form.Label>
                            <Form.Control type="number" value={amount} onChange={e => setAmount(e.target.value)} required/>
                        </Form.Group>
                        <Button variant="primary" type="submit" className="button2 font-weight-bold" block>Submit</Button>
                    </Form>
                </Modal.Body>
                
            </Modal>
        </>

    )
    
    
};
