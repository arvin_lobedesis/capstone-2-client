import { Table} from 'react-bootstrap'
import Moment from 'react-moment';
import UserContext from '../UserContext'
import {useContext, useState, useEffect,} from 'react'
import {TextField, MenuItem, Button} from '@material-ui/core'
import DeleteButton from './DeleteButton'
import UpdateButton from './UpdateButton'
import TableData from './TableData'


export default function BudgetTable({budgets, setBudgets,}) {
    // console.log(budgets)

    return(
        <Table className="mt-4"  hover responsive>
            <thead>
                <tr className="bg-primary text-light">
                    <th className = "tableData">Action</th>
                    <th className = "tableData">Name</th>
                    <th className = "tableData">Category</th>
                    <th className = "tableData">Amount</th>
                    <th className = "tableData">Type</th>
                    <th className = "tableData">Description</th>
                </tr>
            </thead>
            <tbody>
                {
                    budgets.map(budget => {
                        return(
                            <TableData key={budget._id} budget={budget} setBudgets={setBudgets} />
                        )
                    })
                }
            </tbody>
        </Table>
    )
}
