import {useState, useContext} from 'react'
import {Tooltip, IconButton} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete';
import Router from 'next/router'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function DeleteButton({budgetId, setBudgets}) {

    const {user, setBalance} = useContext(UserContext)

    const archive = () => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/budgets/${budgetId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
                    headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
                })
                .then(res => res.json())
                .then(data => {
                    // console.log(data, "deleteButton")
                    Router.reload()

                    const computeActiveBalance = () => {
                        const activeBudgets = data.budgets.filter(budget => budget.isActive=== true)
                        setBudgets(activeBudgets)
                        return activeBudgets.reduce((accumulatedValue, currentBudget) => {
                            if(currentBudget.type === "Expense"){
                                return +accumulatedValue - +currentBudget.amount
                            } else {
                                return +accumulatedValue + +currentBudget.amount
                            }
                        }, 0)
                    }
                    setBalance(computeActiveBalance())
                    
                })
            }
        })
    }

    return (
        <Tooltip title="Delete" placement="left">
            <IconButton aria-label="delete" onClick={archive}>
                <DeleteIcon/>
            </IconButton>
        </Tooltip>
    )
    
};
