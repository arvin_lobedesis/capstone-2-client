import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import {Doughnut} from 'react-chartjs-2'
import {Modal, Button} from 'react-bootstrap'

export default function DoughnutChart() {

    const {user, budgets} = useContext(UserContext)
    const [show, setShow] = useState(false);

    const filterIncome = budgets.filter(budget => budget.isActive === true && budget.type === 'Income')
    const filterExpense = budgets.filter(budget => budget.isActive === true && budget.type === 'Expense')
    
    const income = filterIncome.length
    const expense = filterExpense.length
    
    
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true);

    console.log(income,expense)

    return(
        <>
            <Button variant="primary" onClick={handleShow}>Show Chart</Button>
                

            <Modal
                show={show}
                onHide={handleClose}
                keyboard={false}
                centered
            >
                <Modal.Header closeButton>
                <Modal.Title>Income vs Expense</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Doughnut
                    data={{
                        datasets:[{
                            data: [income,expense],
                            backgroundColor: ["green", "red"]
                        }],
                        labels: [
                            'Income',
                            'Expense'

                        ]
                    }}

                    redraw={false}

                />
                </Modal.Body>
                
            </Modal>
        </>
    )
    
};
