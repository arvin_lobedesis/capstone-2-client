import {useContext} from 'react';
import UserContext from '../UserContext';
import Router from 'next/router';
import {GoogleLogin} from 'react-google-login';
import Swal from 'sweetalert2'


export default function GoogleLoginButton(){
	const {setUser} = useContext(UserContext)

	const captureLoginResponse = (response) => {
		// console.log(response)
		const payload = {
			method: 'POST',
			headers: {'Content-Type': 'application/json' } ,
			body: JSON.stringify({tokenId: response.tokenId})
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/verify-google-id-token`, payload)
		.then(res => res.json())
		.then(data => {
			if(typeof data.accessToken !== 'undefined'){
				localStorage.setItem('token', data.accessToken)
				setUser({
					id: data._id,
					
				})
				Router.push('/budgets')
			} else{
				if(data.error == 'google-auth-error'){
					Swal.fire(
						'Google Auth Error',
						'Google authentication procedure failed.', 
						'error'
						)
				}else if(data.error === 'login-type-error'){
					Swal.fire(
						'Login Type Error',
						'You may have registered through a different login procedure.',
						'error'
						)
				}
			}
		})
	}

	return(
		<GoogleLogin 
		clientId='528896774469-h2h71im6b9es2k3l4act6iruasqb3qhk.apps.googleusercontent.com' 
		onSuccess={captureLoginResponse} 
		onFailure={captureLoginResponse} 
        cookiePolicy={'single_host_origin'}
        className="button2 rounded"
	    buttonText="Sign in using Google"
		/>


		)
}