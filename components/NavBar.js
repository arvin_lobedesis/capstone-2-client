import {Navbar, Nav, NavDropdown, Row, Col} from 'react-bootstrap'
import {useContext} from 'react'
import UserContext from '../UserContext'

export default function NavBar() {

    const {balance} = useContext(UserContext)
    return(
        <Navbar expand="lg" sticky="top" className="navbar">
            <Navbar.Brand href="/budgets"><img src="/images/isbudget.png" className="isbudget d-inline-block align-top"/></Navbar.Brand>
                <Nav className="mr-auto navdrop">
                    <NavDropdown title="" id="basic-nav-dropdown">
                        <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
                <Nav>
                    <Nav.Item className="text-center h4 d-flex text-border"><span className="ml-4 mr-1">Balance: </span>&#8369;{balance} </Nav.Item>
                </Nav>
        </Navbar>
    )
}
