import {Modal, Button, Form, Row, Col} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import Router from 'next/router'
import Swal from 'sweetalert2'



export default function RegisterForm() {
    const [isActive, setIsActive] = useState(false)
    const [show, setShow] = useState(true);
    
    const handleClose = () => Router.push('/login')
    const handleShow = () => setShow(true);

	  const [userName, setUserName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    
    useEffect(()=>{
        // additional validations for the password and mobile on fields
        if(password !== '') {
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [password])

    function registerUser(e){
		e.preventDefault()

		// send fetch request to API endpoint responsible for creating a new user
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/register`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				userName: userName,
				email: email,
        password: password
                
			})
		})
		.then(res => res.json())
		.then(data => {
            // console.log(data)
			if(data === true){
        Router.push('/login')
			}else{
				Swal.fire('Alert!', 'Email has already been registered', 'error')
			}
		})
	}
    
    return (
    
          <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
            centered
          >

            <Modal.Header closeButton>
              <Modal.Title>Create an account</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group>
                  <Form.Label>Username</Form.Label>
                  <Form.Control type="text" value={userName} placeholder="Username" onChange={e => setUserName(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="email" value={email} placeholder="Email" onChange={e => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" value={password} placeholder="Password" onChange={e => setPassword(e.target.value)} required/>
                </Form.Group>
                {isActive === true
                  ? <Button variant="primary" type="submit" className="button2 font-weight-bold">Submit</Button>
                  : <Button variant="primary" type="submit" className="button2 font-weight-bold" disabled>Submit</Button>}
              </Form>
            </Modal.Body>
            
          </Modal>
        
      )
}
