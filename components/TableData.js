import { Table } from 'react-bootstrap'
import Moment from 'react-moment';
import UserContext from '../UserContext'
import {useContext, useState, useEffect,} from 'react'
import {TextField, MenuItem, Button} from '@material-ui/core'
import DeleteButton from './DeleteButton'
import UpdateButton from './UpdateButton'

export default function TableData({budget, setBudgets,}) {

    const {setBalance} = useContext(UserContext)
	const [canEdit, setCanEdit] = useState(false)
	const [categories, setCategories] = useState([])
	const [name, setName] = useState(budget.name)
    const [categoryId, setCategoryId] = useState(budget.categoryId)
    const [type, setType] = useState(budget.type)
    const [amount, setAmount] = useState(budget.amount)
    const [description, setDescription] = useState(budget.description)

    useEffect(()=> {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories/`)
		.then(res => res.json())
		.then(data => {
			setCategories(data)
			setCanEdit(false)
		})
    }, [])
    
    const handleOnChange = (e) => {
        if(e.target.name === "budgetName"){
            setName(e.target.value)
        } else if (e.target.name === "categoryId") {
            setCategoryId(e.target.value)
        } else if (e.target.name === "categoryType"){
            setType(e.target.value)
        } else if (e.target.name === "amount") {
            setAmount(e.target.value)
        } else if (e.target.name === "description") {
            setDescription(e.target.value)
        }

        
    }

    const handleSubmit = () => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/budgets/${budget._id}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                categoryId: categoryId,
                type: type,
                amount: amount,
                description: description
            })
        })
        .then(res => res.json())
        .then(data => {
            
            const computeActiveBalance = () => {
             const activeBudgets = data.budgets.filter(budget => budget.isActive=== true)
             return activeBudgets.reduce((accumulatedValue, currentBudget) => {
                 if(currentBudget.type === "Expense"){
                     return +accumulatedValue - +currentBudget.amount
                 } else {
                     return +accumulatedValue + +currentBudget.amount
                 }
             }, 0)
         }
             setBalance(computeActiveBalance())
             setBudgets(data.budgets.filter(budget => budget.isActive === true))
             setCanEdit(false)
        })
    }

    return(
        <>
                
        <tr key={budget._id}>
            <td className = "tableData">
                <div className="action">
                    <DeleteButton budgetId={budget._id} setBudgets={setBudgets}/>
                    <UpdateButton 
                        canEdit = {canEdit} 
                        setCanEdit = {setCanEdit}
                        setName = {setName}
                        name = {name}
                        type = {type}
                        categoryId = {categoryId}
                        amount = {amount}
                        description = {description}
                        setCategoryId = {setCategoryId}
                        setType = {setType}
                        setAmount = {setAmount}
                        setDescription = {setDescription}
                    />
                    { 
                        canEdit && <Button onClick={handleSubmit}>Submit</Button>
                    }
                </div>
            </td>
            <td className = "tableData">
            {
                canEdit ? <TextField onChange = {handleOnChange} name="budgetName" label = "Budget Name" defaultValue={budget.name}></TextField> : budget.name
            }
            </td>
            <td className = "tableData">
            {
                canEdit 
                ? <TextField
                    defaultValue = {budget.categoryId._id}
                    name="categoryId"
                    onChange = {handleOnChange}
                    select
                    label="Category">
                            { 
                                categories.map(category => {
                                return (
                                        <MenuItem key = {category._id} value={category._id}>{category.name}</MenuItem>
                                    )
                            }) 
                            }
                    </TextField>

                : budget.categoryId.name
            }
            </td>
            <td className = "tableData">
            { 
                canEdit 
                ? <TextField defaultValue = {budget.amount} onChange = {handleOnChange} name="amount" label="Amount" type="number" />
                : budget.amount
            }
            </td>
            <td className = "tableData">
            {	
                canEdit 
                ? 	<TextField
                    defaultValue = {budget.type}
                    name = "categoryType"
                    label="Type"
                    onChange = {handleOnChange}
                    select>
                        <MenuItem value="Expense">Expense</MenuItem>
                        <MenuItem value="Income">Income</MenuItem>
                    </TextField> 
                : budget.type
            }
            </td>
            <td style={{maxWidth: "40%"}} className = "tableData">
            {	
                canEdit 
                ? <TextField 
                    style = {{overflow: "hidden"}}
                    defaultValue = {budget.description}
                    onChange = {handleOnChange}
                    name = "description" 
                    label = "description"
                    />
                :budget.description
            }
            </td>
        </tr>
                        
        </>       
    )
    
};
