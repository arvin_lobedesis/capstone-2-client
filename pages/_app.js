import '../styles/globals.css'
import {useState, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Head from 'next/head'
import { UserProvider } from '../UserContext';
import Router from 'next/router'

function MyApp({ Component, pageProps }) {
	
	const [user, setUser] = useState({
		id: null,
		userName: '',
		budgets: [],
		isAdmin: null
	})
	const [balance, setBalance] = useState(0)
	const [budgets, setBudgets] = useState([])
	const [categories, setCategories] = useState('')

	// function for clearing local storage upon logout
	const unsetUser = ()=>{
		// remove the JWT that was stored in the local storage of app
		localStorage.clear()
		// set the global user state properties to null
		setUser({
			id: null,
			userName: '',
			budgets: [],
			isAdmin: null
		})
		setBalance(0)
		setBudgets([])
		setCategories('')

		Router.push
	}

	useEffect(()=>{
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		// parse the response into desired file type
		.then(res => res.json())
		.then(data => {
		// console.log(data, 'data')
			if(data._id){
				const computeActiveBalance = () => {
					const activeBudgets = data.budgets.filter(budget => budget.isActive===true)
					return activeBudgets.reduce((accumulatedValue, currentBudget) => {
						if(currentBudget.type === "Expense"){
							return +accumulatedValue - + currentBudget.amount
						}else{
							return +accumulatedValue + + currentBudget.amount
						}
					}, 0)
				}
				setUser({
					id: data._id,
					userName: data.userName,
					budgets: data.budgets,
					isAdmin: data.isAdmin
				})
				setBalance(computeActiveBalance())
				setBudgets(data.budgets)
				setCategories(data.categories)
			}else{
				setUser({
					id: null,
					userName: '',
					budgets: '',
					isAdmin: null
					
				})
				setBalance(0)
				setBudgets([])
				setCategories('')
			}

		})
	}, [user.id])
  return (
		<>
			<Head>
				<title>isBudget</title>
				<link rel="icon" href="/isBudget.png" />
				<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
				<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
			</Head>
			<UserProvider value={{user, setUser, unsetUser, balance, setBalance, budgets}}>
				<Component {...pageProps} />
			</UserProvider>
			
		</>
  	)

}

export default MyApp
