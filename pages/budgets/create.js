import AddBudget from '../../components/AddBudget'
import {Card, Col} from 'react-bootstrap'
import NavBar from '../../components/NavBar'

export default function create() {
    return(
        <>
            <NavBar/>
            <Col className="my-3" lg={{span:6,offset:3}}>
                
                    <AddBudget/>
                
            </Col>
        
        </>
    )
};
