import Navbar from '../../components/NavBar'
import { useState, useEffect, useContext } from 'react'
import UserContext from '../../UserContext'
import {Container, Button, Row, Col, Card, Form} from 'react-bootstrap'
import Router from 'next/router'
import BudgetTable from '../../components/BudgetTable'
import { useScrollTrigger } from '@material-ui/core';
import AddCategory from '../../components/AddCategory'
import Swal from 'sweetalert2'
import DoughnutChart from '../../components/DoughnutChart'

export default function index() {

    const {user, balance, setBalance } = useContext(UserContext)

    const [name, setName] = useState('')
    const [categoryId, setCategoryId] = useState('')
    const [type, setType] = useState('')
	const [amount, setAmount] = useState('')
    const [description, setDescription] = useState('')
    

    const [budgets, setBudgets] = useState([])
    const [categories, setCategories] = useState([])

    function createBudget(e){
        if(name !== "" && categoryId !== "" && type !== "" && amount !== ""){
            e.preventDefault()
            const newBalance = () => {
                if(type === "Expense"){
                    return +balance - +amount
                } else {
                    return +balance + +amount
                }
            }
            
            const balanceToServer = newBalance()
    
            fetch(`${process.env.NEXT_PUBLIC_API_URL}/budgets`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    name: name,
                    categoryId: categoryId,
                    type: type,
                    amount: amount,
                    description: description
                    
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data===false){
                    Swal.fire('Data Error', 'Something went wrong when fetching the data', 'error')
                }else{
                    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${data}`, {
                        method: "PATCH",
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        },
                        body: JSON.stringify({
                            balance: balanceToServer
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        if(data===false){
                            Swal.fire('Data Error','Something went wrong when fetching the data','error')
                        }else{
                            setBudgets(data.budgets.filter(budget => budget.isActive === true))
                            setBalance(data.balance)
                            Swal.fire({
                                position: 'top',
                                icon: 'success',
                                title: 'New Budget has been added',
                                showConfirmButton: false,
                                timer: 1000
                              })
                            Router.reload()
                        }
                    })
                }
            })
        }
    }

    const handleOnChange = (e) => {
        if(e.target.name === "budgetName"){
            setName(e.target.value)
        } else if (e.target.name === "categoryId") {
            setCategoryId(e.target.value)
        } else if (e.target.name === "categoryType"){
            setType(e.target.value)
        } else if (e.target.name === "amount") {
            setAmount(e.target.value)
        } else if (e.target.name === "description") {
            setDescription(e.target.value)
        }
    }

    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories`, {
  				headers: {
  					'Authorization': `Bearer ${localStorage.getItem('token')}`
  				}
  			})
  			.then(res => res.json())
  			.then(data => {
  				// console.log(data)
  				setCategories(data)
  				fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`,{
                      
  					headers: {
  						'Authorization': `Bearer ${localStorage.getItem('token')}`
  					}
  				})
  				.then(res => res.json())
  				.then(data => {
  					// console.log(data.budgets.filter(budget => budget.isActive === true), "Budgets")
  					setBudgets(data.budgets.filter(budget => budget.isActive === true))
  				})
  			})
    }, [])

    
    
    return(
        <>
            <Navbar/>
            <Container fluid>
                <Col xs md={{offset:4}} lg={{span:8,offset:4}}><h1 className="font-weight-bold">Welcome, {user.userName}</h1></Col>
                
                <Row className="my-5">
                    <Col xs={12} lg={4}>
                        <Card className="my-3 shadow">
                            <Card.Body>
                            <Card.Title className="font-weight-bold">Create Budget</Card.Title>
                            <Form onSubmit={createBudget}>
                                <Form.Group>
                                    <Form.Label>Budget Name</Form.Label>
                                    <Form.Control type="text" value={name} name="budgetName" onChange={handleOnChange} required/>
                                </Form.Group>
                                <Form.Group>
                                    <Row>
                                        <Col xs={7}>
                                            <Form.Label>Category</Form.Label>
                                            <Form.Control as="select" value={categoryId} name="categoryId" onChange={handleOnChange} required>
                                                <option value="" disabled></option>
                                            {	categories.length > 0 
                                                ? categories.map(category => {
                                                return (
                                                        <option key = {category._id} value={category._id}>{category.name}</option>
                                                    )
                                            }) : 
                                                <option>No Categories Listed Yet</option>
                                            }
                                            </Form.Control>
                                        </Col>
                                        <Col xs={5}>
                                            <AddCategory setCategories={setCategories}/>
                                            
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <Row>
                                    <Col>
                                        <Form.Group>
                                            <Form.Label>Category Type</Form.Label>
                                            <Form.Control as="select" value={type} name="categoryType" onChange={handleOnChange} required>
                                                <option value="" name="" disabled></option>
                                                <option value="Income" name="Income">Income</option>
                                                <option value="Expense" name="Expense">Expense</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>
                                    <Col>
                                        <Form.Group>
                                            <Form.Label>Amount</Form.Label>
                                            <Form.Control type="number" value={amount} name="amount" onChange={handleOnChange} required/>
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <Form.Group>
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control as="textarea" rows="3" value={description} name="description" onChange={handleOnChange}/>
                                </Form.Group>
                                <Button variant="primary" type="submit" className="button2 my-3 font-weight-bold" block>Submit</Button>
                            </Form>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={12} lg={8}>
                        <Card className="my-3 shadow">
                            <Card.Body>
                                <Row>
                                    <Col><Card.Title className="font-weight-bold">My Budget</Card.Title></Col>
                                    <Col xs={{span:5,offset:1}} lg={{span:2,offset:4}}><DoughnutChart/></Col>
                                </Row>
                                <BudgetTable budgets={budgets} setBudgets={setBudgets} />
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>   
                

            
        </>
    )
}
    

