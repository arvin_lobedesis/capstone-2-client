import {Jumbotron} from 'react-bootstrap';

export default function error() {
	return(
		<Jumbotron>
			<h1>Oops!</h1>
			<p>Something went wrong! Please try again later.</p>
		</Jumbotron>

		)
}