import {Row, Col, Button, Image, Container} from 'react-bootstrap'
import Link from 'next/link'


export default function Home() {
  return (
    <>
    <Container>
      <Row className="mt-5">
        <Col sm={6} lg={3} className="offset-lg-4 offset-md-3 text-center">
          <Image src="/images/isbudget-logo.png" fluid/>
          <Image src="/images/isbudget.png" fluid className="ml-md-3"/>
        </Col>
        <Col lg={7} className="text-center offset-lg-2">
          <h2 className="head2">Your friendly and easy budget tracking app</h2>
          <Button variant="danger" className="button rounded font-weight-bold ml-lg-4 mt-3">
            <Link href="/login">
              <span>Start Now </span>
            </Link>  
          </Button>
        </Col>
      </Row>
    </Container>
    <Image src="/images/coinsbg.png" className="imagebg d-none d-lg-block"/>
    <Image src="/images/coins-bg.png" className="d-none d-md-block d-lg-none"/>
    </>
  )
}