import {Row, Col, Button, Card, Container, Form, InputGroup} from 'react-bootstrap'
import {useState, useContext, useEffect} from 'react';
import UserContext from '../UserContext';
import Router from 'next/router';
import Link from 'next/link'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { AccountCircle, Lock} from '@material-ui/icons';
import GoogleLoginButton from '../components/GoogleLoginButton'


const useStyles = makeStyles((theme) => ({
    root: {
      margin: {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
  }));
  
export default function login(){
	const classes = useStyles();
    const {setUser} = useContext(UserContext)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)
	
	useEffect(()=>{
        // additional validations for the password and mobile on fields
        if(password !== '' && email !== '') {
            setIsActive(true)
        }else{
            setIsActive(false)
        }
	}, [password, email])
	
	function authenticate(e){
		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => { 
			// console.log(data)
			if(data.accessToken){
				localStorage.setItem('token', data.accessToken)
				fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					setUser({
						id: data._id,
						
					})
					Router.push('/budgets')
				})

			}else{
				Router.push('/error')
			}
		})

	}

	

	return(
		<Container fluid>
			
			<Row className="my-5 justify-content-md-center">
				<Col lg={6} >
					<h2 className="mb-5 text-center font-weight-bolder">Sign in to your isBudget account</h2>
					<Card className="shadow">
					<Form onSubmit={(e) => authenticate(e)} className="my-5">
						<Col lg={{offset:3}}>
							<InputGroup>
								<Row>
									<Col xs={1} className="mt-2">
										<InputGroup.Prepend>
											<AccountCircle style={{fontSize: 40}}  />  
										</InputGroup.Prepend>
									</Col>
									<Col xs={9} className="ml-3 mb-3">
										<TextField id="outlined-email" style={{width:'250px'}} label="Email" variant="outlined" fullWidth type="email" value={email} onChange={(e)=>setEmail(e.target.value)}/>
									</Col>
								</Row>
							</InputGroup>
							<InputGroup>
								<Row>
									<Col xs={1} className="mt-2">
										<InputGroup.Prepend>
											<Lock style={{fontSize: 40}} />  
										</InputGroup.Prepend>
									</Col>
									<Col xs={9} className="ml-3 mb-3">
										<TextField id="outlined-password" style={{width:'250px'}} label="Password" variant="outlined" fullWidth type="password" value={password} onChange={(e)=>setPassword(e.target.value)}/>
									</Col>
								</Row>
							</InputGroup>
						</Col>
							<Row>
								<Col lg={{span:6, offset:3}} className="text-center">
									{isActive === true
									?<button type="submit" className="button rounded my-3 btn btn-danger font-weight-bold"><span>Login </span></button>
									:<button type="submit" className="button rounded my-3 btn btn-danger font-weight-bold" disabled><span>Login </span></button>
									}
									<p>Dont have an account?
										<span className="ml-1">
											<Link href="/register">
											<a className="txt-primary" role="button">Sign up</a>
											</Link>
										</span>
									</p>
									<hr/>
									<GoogleLoginButton/>
								</Col>
							</Row>
					</Form>
					</Card>
				</Col>	
			</Row>
			
		</Container>
			
		)
}