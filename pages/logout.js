import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';
import Router from 'next/router'

export default function logout(){
	// access the globally available unsetUser function by consuming the UserContext object
	const {unsetUser} = useContext(UserContext)

	// invoke unsetUser upon mounting this component
	useEffect(()=> {
		unsetUser()
		Router.push('/')
	},[])
	return null
}