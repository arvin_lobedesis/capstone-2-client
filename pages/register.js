import RegisterForm from '../components/RegisterForm'
import {Image} from 'react-bootstrap'

export default function register() {
    return(
        <>
            <RegisterForm/>
            <Image src="/images/coinsbg.png" className="imagebg d-none d-md-block"/>
        </>
    )
};
